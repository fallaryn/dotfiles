let
  accounts = import ./modules/accounts.nix;
  android = import ./modules/android.nix;
  audio = import ./modules/audio.nix;
  bluetooth = import ./modules/bluetooth.nix;
  corectrl = import ./modules/corectrl.nix;
  dconf = import ./modules/dconf.nix;
  disks = import ./modules/disks.nix;
  doas = import ./modules/doas.nix;
  environment = import ./modules/environment.nix;
  firejail = import ./modules/firejail.nix;
  fonts = import ./modules/fonts.nix;
  gvfs = import ./modules/gvfs.nix;
  home-manager = import ./modules/home-manager.nix;
  jellyfin = import ./modules/jellyfin.nix;
  locale = import ./modules/locale.nix;
  network = import ./modules/network.nix;
  nix = import ./modules/nix.nix;
  ollama = import ./modules/ollama.nix;
  plasma = import ./modules/plasma.nix;
  printing = import ./modules/printing.nix;
  sddm = import ./modules/sddm.nix;
  ssh = import ./modules/ssh.nix;
  steam = import ./modules/steam.nix;
  syncthing = import ./modules/syncthing.nix;
  sysstat = import ./modules/sysstat.nix;
  system = import ./modules/system.nix;
  virtualization = import ./modules/virtualization.nix;
  xserver = import ./modules/xserver.nix;
in {
  flake = {
    nixosModules = {
      inherit
        accounts
        android
        audio
        bluetooth
        corectrl
        dconf
        disks
        doas
        environment
        firejail
        fonts
        gvfs
        home-manager
        jellyfin
        locale
        network
        nix
        ollama
        plasma
        printing
        sddm
        ssh
        steam
        syncthing
        sysstat
        system
        virtualization
        xserver
        ;
      desktop = {
        imports = [
          accounts
          android
          audio
          bluetooth
          corectrl
          dconf
          disks
          doas
          environment
          firejail
          fonts
          gvfs
          home-manager
          jellyfin
          locale
          network
          nix
          # ollama
          plasma
          printing
          sddm
          ssh
          steam
          syncthing
          sysstat
          system
          virtualization
          xserver
        ];
      };
    };
  };
}
