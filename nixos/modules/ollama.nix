{flake, ...}: let
  inherit (flake.config.service.instance) ollama web;
  service = ollama;
  localhost = web.localhost.address0;
in {
  services = {
    ollama = {
      acceleration = false;
      enable = true;
      group = service.name;
      host = "http://${localhost}";
      user = service.name;
    };
    open-webui = {
      enable = true;
      host = localhost;
      port = service.ports.port0;
      environment = {
        ENABLE_OLLAMA_API = "True";
        ANONYMIZED_TELEMETRY = "False";
        DO_NOT_TRACK = "True";
        SCARF_NO_ANALYTICS = "True";
        OLLAMA_BASE_URL = "http://${localhost}:${toString service.ports.port1}";
        WEBUI_AUTH = "True";
      };
    };
  };

  networking = {
    firewall = {
      allowedTCPPorts = [
        service.ports.port0
        service.ports.port1
      ];
    };
  };
}
