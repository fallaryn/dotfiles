{flake, ...}: let
  inherit (flake.config.system.device) nas phone;
  inherit (flake.config.service.instance) syncthing web;
  service = syncthing;
  localhost = web.localhost.address0;
in {
  services = {
    syncthing = {
      enable = true;
      overrideDevices = false;
      overrideFolders = false;
      openDefaultPorts = true;
      systemService = true;
      guiAddress = "${localhost}:${toString service.ports.port0}";
      settings = {
        devices = {
          ${nas.name} = {
            autoAcceptFolders = true;
            name = nas.name;
            id = nas.sync;
          };
          ${phone.name} = {
            autoAcceptFolders = true;
            name = phone.name;
            id = phone.sync;
          };
        };
      };
    };
  };

  networking = {
    firewall = {
      allowedTCPPorts = [
        service.ports.port0
        service.ports.port1
        service.ports.port2
      ];
    };
  };
}
