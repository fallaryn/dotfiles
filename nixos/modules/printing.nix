{pkgs, ...}: {
  services = {
    printing = {
      enable = true;
      drivers = with pkgs; [
        brlaser
        cups-brother-hll2375dw
      ];
    };
  };
}
