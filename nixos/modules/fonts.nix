{pkgs, ...}: {
  fonts = {
    fontconfig.defaultFonts.emoji = ["Noto Fonts Color Emoji"];
    packages = with pkgs; [
      noto-fonts-color-emoji
      open-dyslexic
    ];
  };
}
