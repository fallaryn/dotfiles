{
  config,
  flake,
  pkgs,
  ...
}: let
  inherit (flake.config.people) user0;
  inherit (flake.config.people.user.${user0}) name paths;
in {
  users = {
    users.${user0} = {
      description = name;
      isNormalUser = true;
      shell = pkgs.nushell;
      extraGroups = [
        "adbusers"
        "disk"
        "libvirtd"
        "minecraft"
        "netdev"
        "networkmanager"
        "ollama"
        "syncthing"
        "vboxusers"
        "wheel"
      ];
    };
  };
  home-manager.users = {
    ${user0} = {
      home = {
        username = user0;
        homeDirectory = "/home/${user0}";
        file = {
          "./justfile" = {
            source = ./files/justfile;
          };
        };
        sessionVariables = {
          WLR_NO_HARDWARE_CURSORS = "1";
          WLR_DRM_NO_ATOMIC = "1";
          VIDEO_PLAYER = "vlc";
          EDITOR = "vscode";
          NIXPKGS_ALLOW_INSECURE = "1";
        };
      };
      imports = [
        {home.stateVersion = config.system.stateVersion;}
        (
          import ./config.nix {flake = flake;}
        )
      ];
    };
  };
  systemd.tmpfiles = let
    directoriesAddedToHome = [
      "Projects"
    ];

    directoriesRemovedFromHome = [
      "Music"
      "Public"
      "Templates"
    ];
  in {
    rules =
      [
        "d ${paths.path0}/Projects 0755 ${user0} users -"
        "Z ${paths.path0}/.ssh 700 ${user0} users -"
      ]
      ++ (map (path: "d /home/${user0}/${path} 0755 ${user0} users -") directoriesAddedToHome)
      ++ (map (path: "R /home/${user0}/${path} 0755 ${user0} users - -") directoriesRemovedFromHome);
  };
}
