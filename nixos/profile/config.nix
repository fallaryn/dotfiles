{flake, ...}: let
  inherit (flake) self;
  moduleNames = [
    "creativity"
    "fun"
    "internet"
    "system"
    "terminal"
    "themes"
  ];

  moduleImports = map (module: self.homeModules.${module}) moduleNames;
in {
  imports = moduleImports;
}
