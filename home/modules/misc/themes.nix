{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      openrgb
      catppuccin
      catppuccin-gtk
      ;
  };
}
