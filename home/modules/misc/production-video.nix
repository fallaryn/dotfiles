{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      deskreen
      droidcam
      glaxnimate
      kdenlive
      ;
  };
}
