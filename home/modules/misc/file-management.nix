{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      bulky
      celeste
      flameshot
      gnome-calculator
      gnome-disk-utility
      gparted
      synology-drive-client
      usbimager
      ;
  };
}
