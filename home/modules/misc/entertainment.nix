{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      beets
      flameshot
      mpv
      prismlauncher
      spotify
      vlc
      ;
  };
}
