{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      blender
      darktable
      gimp
      inkscape
      krita
      opentabletdriver
      ;
    inherit
      (pkgs.sweethome3d)
      application
      ;
  };
}
