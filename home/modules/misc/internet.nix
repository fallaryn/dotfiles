{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      bitwarden
      discord
      element-desktop
      firefox
      qbittorrent
      signal-desktop
      tdesktop
      teams-for-linux
      tor-browser
      whatsapp-for-linux
      xdg-utils
      zoom-us
      ;
  };
}
