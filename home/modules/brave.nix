{
  programs.brave = {
    enable = true;
    extensions = [
      {id = "cjpalhdlnbpafiamejdnhcphjbkeiagm";} # uBlock Origin
      {id = "dlnpfhfhmkiebpnlllpehlmklgdggbhn";} # Don't Close Last Tab
      {id = "iplffkdpngmdjhlpjmppncnlhomiipha";} # Unpaywall
      {id = "mnjggcdmjocbbbhaepdhchncahnbgone";} # SponsorBlock
      {id = "ponfpcnoihfmfllpaingbgckeeldkhle";} # YouTube Enhancer
    ];
  };
}
