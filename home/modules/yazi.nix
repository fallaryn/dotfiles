{
  programs.yazi = {
    enable = true;
    enableNushellIntegration = true;
    keymap.manager.prepend_keymap = [
      {
        exec = "quit";
        on = ["Q"];
      }
      {
        exec = "quit --no-cwd-file";
        on = ["q"];
      }
    ];

    settings = {
      log = {
        enabled = false;
      };
      manager = {
        show_hidden = true;
        sort_by = "alphabetical";
        sort_dir_first = true;
        sort_reverse = false;
      };
      preview = {
        tab_size = 2;
        max_width = 1500;
        max_height = 1000;
      };
    };
  };
}
# Keymap
# https://github.com/sxyazi/yazi/blob/main/yazi-config/preset/keymap.toml

