let
  # Folders
  steam = import ./modules/steam;
  # Files
  bottom = import ./modules/bottom.nix;
  brave = import ./modules/brave.nix;
  cursor = import ./modules/cursor.nix;
  direnv = import ./modules/direnv.nix;
  git = import ./modules/git.nix;
  gtk = import ./modules/gtk.nix;
  home-manager = import ./modules/home-manager.nix;
  lazygit = import ./modules/lazygit.nix;
  misc-android = import ./modules/misc/android.nix;
  misc-commandLine = import ./modules/misc/command-line.nix;
  misc-entertainment = import ./modules/misc/entertainment.nix;
  misc-fileManagement = import ./modules/misc/file-management.nix;
  misc-internet = import ./modules/misc/internet.nix;
  misc-productionArt = import ./modules/misc/production-art.nix;
  misc-productionAudio = import ./modules/misc/production-audio.nix;
  misc-productionVideo = import ./modules/misc/production-video.nix;
  misc-productionWriting = import ./modules/misc/production-writing.nix;
  misc-themes = import ./modules/misc/themes.nix;
  misc-virtualization = import ./modules/misc/virtualization.nix;
  mpv = import ./modules/mpv.nix;
  nushell = import ./modules/nushell.nix;
  obs-studio = import ./modules/obs-studio.nix;
  playerctld = import ./modules/playerctld.nix;
  starship = import ./modules/starship.nix;
  vscode = import ./modules/vscode.nix;
  wezterm = import ./modules/wezterm.nix;
  yazi = import ./modules/yazi.nix;
  zellij = import ./modules/zellij.nix;
  zoxide = import ./modules/zoxide.nix;
in {
  flake.homeModules = {
    inherit
      bottom
      brave
      cursor
      direnv
      git
      gtk
      home-manager
      lazygit
      misc-android
      misc-commandLine
      misc-entertainment
      misc-fileManagement
      misc-internet
      misc-productionArt
      misc-productionAudio
      misc-productionVideo
      misc-productionWriting
      misc-themes
      misc-virtualization
      mpv
      nushell
      obs-studio
      playerctld
      starship
      steam
      vscode
      wezterm
      yazi
      zellij
      zoxide
      ;
    creativity = {
      imports = [
        misc-productionArt
        misc-productionAudio
        misc-productionVideo
        misc-productionWriting
        obs-studio
        vscode
      ];
    };
    fun = {
      imports = [
        misc-entertainment
        mpv
        steam
      ];
    };
    internet = {
      imports = [
        brave
      ];
    };
    system = {
      imports = [
        home-manager
        misc-fileManagement
        misc-internet
        misc-virtualization
        playerctld
      ];
    };
    terminal = {
      imports = [
        bottom
        direnv
        git
        lazygit
        misc-android
        misc-commandLine
        nushell
        starship
        wezterm
        yazi
        zellij
        zoxide
      ];
    };
    themes = {
      imports = [
        misc-themes
        cursor
        gtk
      ];
    };
  };
}
