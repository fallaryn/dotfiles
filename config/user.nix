let
  user0 = "stacie";
in {
  inherit
    user0
    ;
  user = {
    "${user0}" = {
      name = "Stacie";
      email = {
        address0 = "staciesimonson@gmail.com";
      };
      paths = {
        path0 = "/home/${user0}"; # Git path
      };
      sshKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILQQDw0NigCC76G/GlHWIMunckaBmfgqbfJXFGWB+8fe stacie@desktop"
      ];
    };
  };
}
