{
  device = {
    # Desktop
    desktop = let
      drivePath = "/run/media";
      byLabel = "/dev/disk/by-label";
      readWritePermissions = ["rw"];
    in {
      label = "Desktop";
      name = "desktop";
      ip = {
        address0 = "192.168.58.104";
      };
      boot = {
        options = [
          "fmask=0022"
          "dmask=0022"
        ];
      };
      storage0 = {
        mount = "${drivePath}/games";
        device = "${byLabel}/Games";
        options = readWritePermissions;
      };
      storage1 = {
        mount = "${drivePath}/entertainment";
        device = "${byLabel}/Entertainment";
        options = readWritePermissions;
      };
    };

    # Synology

    nas = {
      label = "Synology";
      sync = "MWRGX2V-F5XKE5E-REP6ECT-OOPFBMF-22NHSMW-YFBU6MB-PLFUN63-R3MW2QX";
      name = "synology";
    };

    # Pixel 7a

    phone = {
      name = "pixel";
      sync = "RUKSHY4-UCBYRVG-CVYFCMU-M3NLA3Q-JINRF5V-YPR5W32-TEIBJN6-DNQRCAR";
    };
  };
}
