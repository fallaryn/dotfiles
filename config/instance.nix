{
  instance = {
    ollama = let
      ollamaName = "ollama";
      ollamaLabel = "Ollama";
    in {
      label = ollamaLabel;
      name = ollamaName;
      ports = {
        port0 = 8088; # Open-WebUI (Ollama Front End)
        port1 = 11434; # Ollama API
      };
    };
    synology = let
      synologyName = "synology";
      synologyLabel = "Synology";
    in {
      label = synologyLabel;
      name = synologyName;
      ports = {
        port0 = 5001; # Synology HTTPS
      };
    };
    syncthing = let
      syncthingName = "syncthing";
      syncthingLabel = "Syncthing";
    in {
      label = syncthingLabel;
      name = syncthingName;
      ports = {
        port0 = 8388; # Syncthing (WebUI)
        port1 = 21027; # Syncthing (Discovery)
        port2 = 22000; # Syncthing (Transfer)
      };
    };
    web = {
      localhost = {
        address0 = "127.0.0.1"; # Local
        address1 = "0.0.0.0"; # All
        address2 = "192.168.50.1"; # Router
        address3 = "192.168.50.0"; # Router
      };
    };
  };
}
