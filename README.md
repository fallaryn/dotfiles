# REBUILD COMMANDS

Get into dotfiles directory:
cd dotfiles

Update Flake:
nix flake update

Update NixOS:
nixos-rebuild switch --use-remote-sudo --flake ~/dotfiles#desktop

Update Home Manager:
home-manager switch --flake ~/dotfiles#fallaryn

# COMMITTING TO GIT

After **every** editing session, it is prudent and often required that you commit your changes to your GitLab repository in order for rebuilding to work correctly.

1) navigate to the Source Control panel to the left.

2) on the dropdown header that says "Changes", press the big "+" sign to stage your changes.

3) name your commit whatever you want, like "NixOS" or something.

4) hit "Commit", then "Sync Changes".

# ADDING PACKAGES

The procedure is a little different than before. Rather than using the terminal, you'll edit your configs here in VSCode (trust me it's easier).

1) find your packages here: https://search.nixos.org/packages

2) in the Explorer panel on the left, add the desired package to "/nixos/modules/user.nix" under "users.users.stacie. packages = with pkgs; [new package goes here];"

# ADDING NIX OPTIONS

Some packages have cool features that are useful to turn on.

1) check if your package has Nix options here:
	https://search.nixos.org/options

2) if the package has Nix options, you'll want to include this package in your build as an independent module by making a new .nix file under "/nixos/modules"

If you navigate to the Explorer panel on the left, you can see a number of modules in the "/nixos/modules" folder. Opening any of them will give you an idea of how you should be creating your new module.

For example, if you search for "opentabletdriver" on https://search.nixos.org/options, you'll see it has a few options. Now take a look at "/nixos/modules/tablet.nix" to see how these options can be applied.

3) name the .nix file anything you want (but it preferably should be easily identifiable), and place it in the "/nixos/modules" folder.

4) update the "default.nix" file in "/nixos/modules" to include the new file in the list of imports.

# ADDING HOME MANAGER PACKAGES

Some packages have Home Manager options, which are similar to Nix options. The procedure for installing them is identical to the procedure for installing packages with Nix options, but it will be done in "/home-manager/modules" rather than "/nixos/modules" and the "/home-manager/default.nix" file will be edited to include the new package instead of the "/nixos/modules/default.nix" file.

1) check if your package has Home Manager options here: https://mipmip.github.io/home-manager-option-search/

2) if the package has Home Manager options, you'll want to include this package in your build as an independent module by making a new .nix file under "/home-manager/modules"

3) name the .nix file anything you want (but it preferably should be easily identifiable), and place it in the "/home-manager/modules" folder.

4) update the "default.nix" file in "/home-manager/modules" to include the new file in the list of imports.

# ADDING PACKAGES WITH IMPORTS/EXPORTS

Sometimes it's preferred to include files to be imported and/or exported with packages. For example, the Firefox module located in "/home-manager/modules/firefox" has multiple files that are being imported into the "default.nix" file located in the same folder. Likewise, the OBS module located in "/home-manager/modules/obs-studio" is exporting configuration settings to your PC.

Importing files is useful for making files look cleaner and less unwieldy. Exporting files is useful for declaring app and/or system settings so you don't have to do them from scratch with every fresh install.

The commands for importing and exporting files differ depending on what you're trying to do, and is beyond the scope of this tutorial. So, if you need help with those, just message me and I'll see what I can do.